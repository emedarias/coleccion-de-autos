//llamamos al boton "submit" y lo guardamos en la variable agregar
const agregar = document.querySelector("input[type='submit']");
//Le asignamos al boton la funcion "validar"
agregar.addEventListener("click", validar);
//llamamos a los botones "borrar primero" y "borrar ultimo" y lo guardamos en la variable botones;
const botones = Array.from(document.querySelectorAll("input[type='button']"));
//asignamos a "borrar primero" la funcion "borrar_primero";
botones[0].addEventListener("click", borrar_primero);
//asignamos a "borrar ultimo" la funcion "borrar_ultimo";
botones[1].addEventListener("click", borrar_ultimo);
//Creamos un vector vacio donde vamos a guardar las marcas registradas.
let marca = new Array;
//llamamos al campo donde vamos a mostrar las marcas guardadas.
const elegidos = document.querySelector("#mostrar");

function validar(event)
	{
		//Evitamos que el formulario se envie
		event.preventDefault();
		//Guardamos el valor que tiene el input dentro de la variable auto.
		let auto=document.querySelector("input[type='text']").value;
		//Borramos el texto que tiene el input,asi, es mas facil escribir y agregar mas.
		document.querySelector("input[type='text']").value="";
		//Si el dato es distinto a vacio.
		if(auto!="")
			{
				//Agregamos el auto al vector de autos.
				marca.push(auto);		
			}
		//llamamos a la funcion "imprimir"
		imprimir();
	}

function borrar_primero()
	{
		//Borramos el primer elemento del Array marca.
		marca.shift();
		//Llamamos a la funcion imprimir
		imprimir();	
	}

function borrar_ultimo()
	{
		//Borramos el ultimo elemento del Array marca.
		marca.pop();
		//Llamamos a la funcion imprimir
		imprimir();	
	}


function imprimir()
	{
		//Limpiamos el parrafo donde se van a imprimir todos los autos.
		elegidos.innerHTML="";
		//Ciclamos por el Array marca, donde estan todos las marcas de autos guardadas.
		for(elemento of marca)
			{
				//Agregamos al parrafo todos las marcas de autos, y ademas le agregamos una coma y un espacio para que quede mas ordenado.
				elegidos.innerHTML+=elemento+", ";

			}
	}